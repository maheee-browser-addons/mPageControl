if (!window.lib) window.lib = {};


lib.createScriptList = function (parent, list, fDelete, fSetForm) {
  for (let key of Object.keys(list || {}).sort()) {
    let script = list[key];

    parent.appendChild(lib.createButton(script.name, () => {
        fSetForm(key);
        lib.executeScriptWithJquery(script.code).then(res => {
          console.log(res);
        }, err => {
          console.error(err);
        });
    }, () => {
      console.log(key);
      fDelete(key);
    }));
  }
};

lib.createButton = function (title, click1, click2) {
  let wrapper = document.createElement('div');
  let button1 = document.createElement('button');
  let button2 = document.createElement('button');

  wrapper.className = 'btn-group script-button';

  button1.className = 'btn btn-default';
  button1.textContent = title;
  button2.className = 'btn btn-default';
  button2.appendChild(lib.createIcon('cancel'));

  button1.onclick = click1;
  button2.onclick = click2;

  wrapper.appendChild(button1);
  wrapper.appendChild(button2);

  return wrapper;
};

lib.createIcon = function (name) {
  let icon = document.createElement("i");
  icon.className = "icon icon-" + name;
  return icon;
};
