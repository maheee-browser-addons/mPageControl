if (!window.lib) window.lib = {};


lib.recreatePopup = function (elementId, setting, fDelete, fSetForm) {
  let parent = document.getElementById(elementId);
  while (parent.firstChild) {
      parent.removeChild(parent.firstChild);
  }
  lib.createScriptList(parent, setting, fDelete, fSetForm);
};

lib.initPopup = function (hostname, settings) {
  let elScriptName = document.getElementById('scriptName');
  let elScriptCode = document.getElementById('scriptCode');
  let elTypeGlobal = document.getElementById('scriptTypeGlobal');
  let elTypePage = document.getElementById('scriptTypePage');

  let createToggle = true;

  function toggleCreateArea() {
    createToggle = !createToggle;
    document.getElementById('createToggleContent').style.display = !createToggle ? 'none' : '';
    document.getElementById('createToggleTrue').style.display = createToggle ? 'none' : '';
    document.getElementById('createToggleFalse').style.display = !createToggle ? 'none' : '';
  }

  toggleCreateArea();

  document.getElementById('createToggle').onclick = toggleCreateArea;

  document.getElementById('buttonTest').onclick = function () {
    lib.executeScriptWithJquery(elScriptCode.value).then(res => {
      console.log(res);
    }, err => {
      console.error(err);
    });
  };
  document.getElementById('buttonSave').onclick = function () {
    let newScript = {
      name: elScriptName.value,
      code: elScriptCode.value
    };
    let key = elScriptName.value;
    let glob = elTypeGlobal.checked;
    let setting = glob ? settings.default : settings.page;

    if (!key || !key.length || !newScript.code || !newScript.code.length) {
      return;
    }

    if (!setting.scripts) {
      setting.scripts = {};
    }
    setting.scripts[key] = newScript;

    if (glob) {
      lib.storeDefaultSettings(setting);
      renderGlobal();
    } else {
      lib.storePageSettings(hostname, setting);
      renderPage();
    }
  };

  function setForm(name, code, glob) {
    elScriptName.value = name;
    elScriptCode.value = code;
    elTypeGlobal.checked = glob;
    elTypePage.checked = !glob;
    if (glob) {
      renderGlobal();
    } else {
      renderPage();
    }
  }

  function globalSetFormScript(key) {
    let script = settings.default.scripts[key];
    setForm(script.name, script.code, true);
  }
  function pageSetFormScript(key) {
    let script = settings.page.scripts[key];
    setForm(script.name, script.code, false);
  }

  function globalDeleteScript(key) {
    let script = settings.default.scripts[key];
    delete settings.default.scripts[key];
    lib.storeDefaultSettings(settings.default);
    setForm(script.name, script.code, true);
  }
  function pageDeleteScript(key) {
    let script = settings.page.scripts[key];
    delete settings.page.scripts[key];
    lib.storePageSettings(hostname, settings.page);
    setForm(script.name, script.code, false);
  }

  function renderGlobal() {
    lib.recreatePopup('globalScripts', settings.default.scripts || {}, globalDeleteScript, globalSetFormScript);
  }
  function renderPage() {
    lib.recreatePopup('pageScripts', settings.page.scripts || {}, pageDeleteScript, pageSetFormScript);
  }

  renderGlobal();
  renderPage();
}


lib.getActiveTabHostname().then((hostname) => {
  lib.retrieveSettings(hostname).then((settings) => {lib.initPopup(hostname, settings);});
});
