if (!window.lib) window.lib = {};


lib.executeScriptWithJquery = function (code) {
  return new Promise((resolve, reject) => {
    lib.executeJquery().then(res => {
      lib.executeScript(code).then(resolve, reject);
    }, reject);
  });
};

lib.executeJquery = function () {
  return browser.tabs.executeScript({
    allFrames: false,
    file: '/vendor/jquery-3.2.1.slim.min.js'
  });
};

lib.executeScript = function (code) {
  return browser.tabs.executeScript({
    allFrames: false,
    code: code
  });
};

lib.getActiveTabHostname = function () {
  return new Promise((resolve, reject) => {
    browser.tabs.query({active: true}).then(tabs => {
      let hostname = tabs && tabs.length > 0 && lib.getHostnameFromUrl(tabs[0].url);
      resolve(hostname || tabs[0].url || '_undefined-hostname_');
    });
  });
};

lib.getHostnameFromUrl = function (url) {
  if (url) {
    return (new URL(url)).hostname;
  }
  return null;
};

lib.retrieveSettings = function (hostname) {
  return new Promise((resolve, reject) => {
    browser.storage.local.get(['page_' + hostname, 'default']).then(data => {
      resolve({
        page: data['page_' + hostname] || {},
        default: data['default'] || {}
      });
    });
  });
}

lib.storePageSettings = function (hostname, data) {
  let d = {};
  d['page_' + hostname] = data;
  browser.storage.local.set(d);
};

lib.storeDefaultSettings = function (data) {
  let d = {};
  d['default'] = data;
  browser.storage.local.set(d);
};
