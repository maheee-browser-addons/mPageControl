# ![](./plugin/icons/default-48.png) PageControl

## Description

mPageControl is a Firefox Plugin which provides the possibility to add scripts on the fly to modify the body of websites.

Scripts can be stored for individual pages as well as globally.

## Scripts / Examples

For examples check the [Wiki](https://gitlab.com/maheee-browser-addons/mPageControl/wikis/home)

## Screenshots

![](./res/screenshot_1.png)